export const convertToPrice = (value: number) => {
  const twoDecimal = parseFloat(String(value)).toFixed(2);
  const result = twoDecimal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return `${result}`;
};
export const patternValueCount = (value: number) => {
  const result : string = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return `${result}`;
};
