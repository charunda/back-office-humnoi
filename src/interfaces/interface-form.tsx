import { RcFile } from "antd/lib/upload";
import { IProductImage } from "./interface-order";
import { IBrands, ICategories, ICategory } from "./interface-response";
import { IProductLists } from "./interface-table";

export interface IUploadedProductImage {
  src: string;
  color: string;
  amount: number;
}

export interface IProductSelect {
  src: string;
  name: string;
  price: number;
  stock: number;
}

export interface IFormProductStep1 {
  name: string;
  detail: string;
  price: number;
  discount: number;
  qty: number;
  brand_id: number;
  is_active: boolean;
  free_shipping: boolean;
  category_id: number;
}
export interface IFormImageProduct {
  image: any;
  product_id: number;
  color: string;
  stock: number;
  base64: any;
  uid?: string;
  id?: number;
}
export interface IFormCoverImage {
  product_list_id?: number;
  imagePath: any;
  base64: any;
  file: RcFile;
}
export interface IOptionsStep1 {
  optionsBrand: IBrands[];
  optionsCatergory: ICategories[];
}

export interface IProduct {
  id: number;
  name: string;
  price: number;
  price_show: number;
  qty: number;
  free_shipping: boolean;
  is_active: boolean;
  is_best_seller: boolean;
  image_path: string;
  category_id: number;
  brand_id: number;
  detail: string;
  discount: number;
  product_lists: IProductLists;
  freebies?: boolean;
  image_file?: string;
  category_parent_id: number;
  root_parent_id: number
}
export interface IProductRegular {
  id: number;
  name: string;
  price?: number;
  price_show?: number;
  qty?: number;
  stock?: number;
  free_shipping: boolean;
  is_active: boolean;
  is_best_seller: boolean;
  image_path?: string;
  category_id?: number;
  brand_id?: number;
  detail: string;
  discount?: number;
  product_lists: IProductLists[];
  image_file?: string;
}
export interface IProductInSet {
  product_id: number[];
  category_id: number;
  freebies?: boolean;
  products?: IProduct[];
}
export interface IFormProductSet {
  id: number;
  name: string;
  brand_id: number | undefined;
  qty: number;
  price: number;
  discount: number;
  price_show: number;
  detail: string;
  free_shipping: boolean;
  main_product_id: number;
  main_category_id: number;
  is_active: boolean;
  image_path?: string;
  image_file?: string;
  product_in_set: [];
}
export interface IFormLiquid {
  id: number;
  name: string;
  detail: string;
  brand_id?: number;
  smell?: string | number;
  image_file?: string;
  sweet?: number;
  cool?: number;
  category_id?: number;
  discount: number;
  price: number;
  ml: number;
  coil_size?: number;
  symptom?: string;
  shipping_free: boolean;
  free_shipping: boolean; //หลังบ้านส่งมาตอน get id
  is_active: boolean;
  coil_point_eight?: string;
  coil_point_six?: string;
  sweet_point_six?:
    | "หวานน้อยมาก"
    | "หวานน้อย"
    | "หวานปานกลาง"
    | "หวานมาก"
    | undefined; // ทำเพื่อหน้าบ้าน
  sweet_point_eight?:
    | "หวานน้อยมาก"
    | "หวานน้อย"
    | "หวานปานกลาง"
    | "หวานมาก"
    | undefined;
  cool_point_six?:
    | "เย็นน้อยมาก"
    | "เย็นน้อย"
    | "เย็นปานกลาง"
    | "เย็นมาก"
    | undefined;
  cool_point_eight?:
    | "เย็นน้อยมาก"
    | "เย็นน้อย"
    | "เย็นปานกลาง"
    | "เย็นมาก"
    | undefined;
  stock: number;
  qty?: number;
}

export interface IPropsDrawerSelected {
  isOpen: boolean;
  category: ICategory;
  productType: "main" | "secondary" | "";
}
export interface IFormUsers {
  id: number;
  name: string;
  username: string;
  password: string;
  confirm_password: string;
  confirm_new_password: string;
  phone: string;
  user_type_id: number | undefined;
}

export interface IPropsModalUser {
  visible: boolean;
  defaultData: IFormUsers;
  type: "add" | "edit";
}

export interface IFormProductListSingle {
  id: number;
  name: string;
  image_file: string;
  free_shipping: boolean;
  stock: number;
  sold: number;
  product_id: number;
  color: string;
  product_set_id: number;
  product_list_type_id: number;
  product_list_images: IProductImage[];
}
export interface IFormProductSingle {
  id: number;
  name: string;
  detail: string;
  price: number;
  discount: number;
  price_show: number;
  qty: number;
  brand_id: number | undefined;
  free_shipping: boolean;
  is_active: boolean;
  is_best_seller: boolean;
  image_path: string;
  category_id: number | undefined;
  product_lists: IFormProductListSingle[];
}
export interface IFormProductLiquid {
  id: number;
  name: string;
  detail: string;
  price?: number;
  discount?: number;
  price_show?: number;
  qty?: number;
  brand_id?: number;
  free_shipping: boolean;
  shipping_free: boolean;
  is_active: boolean;
  is_best_seller: boolean;
  image_path: string;
  category_id?: number;
  product_lists: IFormLiquid[];
}
export interface IFormProductSaltnic {
  id: number;
  name: string;
  detail: string;
  price?: number;
  discount?: number;
  price_show?: number;
  qty?: number;
  brand_id?: number;
  free_shipping: boolean;
  is_active: boolean;
  is_best_seller: boolean;
  image_path: string;
  category_id?: number;
  coil_point_eight?: string;
  coil_point_six?: string;
  sweet_point_six?:
    | "หวานน้อยมาก"
    | "หวานน้อย"
    | "หวานปานกลาง"
    | "หวานมาก"
    | undefined; // ทำเพื่อหน้าบ้าน
  sweet_point_eight?:
    | "หวานน้อยมาก"
    | "หวานน้อย"
    | "หวานปานกลาง"
    | "หวานมาก"
    | undefined;
  cool_point_six?:
    | "เย็นน้อยมาก"
    | "เย็นน้อย"
    | "เย็นปานกลาง"
    | "เย็นมาก"
    | undefined;
  cool_point_eight?:
    | "เย็นน้อยมาก"
    | "เย็นน้อย"
    | "เย็นปานกลาง"
    | "เย็นมาก"
    | undefined;
}
export interface IPermissionUser {
  id: number;
  type_name: string;
  access_path: string;
}
