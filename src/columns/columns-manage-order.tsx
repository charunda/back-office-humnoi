import React from "react";
import type { ColumnsType } from "antd/lib/table";
import { TagStatusTable } from "src/components";
import { Typography } from "antd";
import dayjs from "dayjs";
import { IRowOrders } from "src/interfaces/interface-table";
import { convertToPrice } from "src/utils/pattern-number";
import { renderStatusTag } from "src/utils/helper";

const { Text } = Typography;
const ColumnsManageOrder: ColumnsType<IRowOrders> = [
  {
    title: "ลำดับ",
    dataIndex: "index",
    key: "index",
    width: 60,
    fixed: true,
    align: "center",
    render: (_value, _item, index) => <span>{index + 1}</span>,
  },
  {
    title: "เลขที่คำสั่งซื้อ",
    dataIndex: "code",
    key: "code",
    width: 100,
    fixed: true,
  },
  {
    title: "ชื่อผู้สั่งสินค้า",
    dataIndex: "customer_name",
    key: "customer_name",
    width: 100,
  },
  {
    title: "วันที่สั่งซื้อ",
    dataIndex: "order_date",
    key: "order_date",
    width: 100,
    render: (_, { order_date }) => (
      <Text>{dayjs(order_date).format("DD/MM/YYYY")}</Text>
    ),
  },
  {
    title: "สถานะ",
    key: "status",
    dataIndex: "status",
    width: 100,
    render: (_, { status_id, status }) => renderStatusTag(status_id, status),
  },
  {
    title: "เลขติดตามพัสดุ",
    dataIndex: "tracking_code",
    key: "tracking_code",
    align: "center",
    width: 100,
    render: (value: string) => {
      return value ? (
        <Text type="success">{value}</Text>
      ) : (
        <Text type="secondary">-ไม่มีเลขพัสดุ-</Text>
      );
    },
  },
  {
    title: "ค่าส่ง (บาท)",
    dataIndex: "total_shipping_price",
    key: "total_shipping_price",
    width: 80,
    align: "right",
    render: (value: number) => <Text>{convertToPrice(value)}</Text>,
  },
  {
    title: "ค่าสินค้า (บาท)",
    dataIndex: "total_price",
    key: "total_price",
    width: 80,
    align: "right",
    render: (value: number) => <Text>{convertToPrice(value)}</Text>,
  },
  {
    title: "ส่วนลด (บาท)",
    dataIndex: "total_discount",
    key: "total_discount",
    width: 80,
    align: "right",
    render: (value: number) => (
      <Text type={value > 0 ? "danger" : "secondary"}>{`- ${convertToPrice(
        value
      )}`}</Text>
    ),
  },
  {
    title: "ยอดชำระ (บาท)",
    dataIndex: "total_net_price",
    key: "total_net_price",
    width: 80,
    align: "right",
    render: (value: number) => <Text strong>{convertToPrice(value)}</Text>,
  },
];

export default ColumnsManageOrder;
