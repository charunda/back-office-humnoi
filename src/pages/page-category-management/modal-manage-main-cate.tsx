import React, { useState } from "react";
import {
  Switch,
  Col,
  Row,
  Spin,
  Drawer,
  Empty,
  Divider,
  Input,
  Form,
  message,
  Modal,
  Typography,
  Table,
} from "antd";
import { ButtonCustom, TagStatus } from "src/components";
import {
  ICategories,
  INewSubCategory,
  ISubCategory,
} from "src/interfaces/interface-response";
import { ModalFormStyled } from "src/styles/modal-styled";

import { API, AXIOS } from "src/services";
import { AxiosError } from "axios";
import { messageAction } from "src/utils/message";
import { propsModalSave } from "src/utils/props-component";
import { EditFilled, PlusCircleOutlined } from "@ant-design/icons";
import { ColumnsType } from "antd/lib/table";
const { Text } = Typography;

const { confirm } = Modal;
interface IPropsModalCaergory {
  modalProps: {
    visible: "main" | "sub" | "";
    parent: ICategories;
  };
  onCloseModal: () => void;
  onFinished: () => void;
}
export default function ModalMangeMainCate({
  modalProps,
  onCloseModal,
  onFinished,
}: IPropsModalCaergory) {
  const { visible } = modalProps;
  const [loading, setLoading] = useState(false);
  const [subCategory, setSubCategory] = useState<ISubCategory[]>([]);
  const [newSub, setnewSub] = useState<ISubCategory>({
    name: "",
    is_active: true,
  });
  const [formItem, setFormItem] = useState<INewSubCategory>({
    parent_id: 0,
    name: "",
    is_active: true,
  });

  const onUpdateSub = () => {
    message.success("บักทึกรายการย่อยสำเร็จ!");
    setnewSub({ name: "", is_active: true });
    setSubCategory([...subCategory, newSub]);
  };

  const onSubmitData = () => {
    confirm({
      ...propsModalSave,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          setLoading(true);
          AXIOS({
            method: "post",
            url: API.CATEGORY,
            data: { ...formItem, sub_categories: subCategory },
          })
            .then(() => {
              messageAction("saveSuccess");
              setLoading(false);
              onCloseDrawer();
              onFinished();
              resolve();
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          setLoading(false);
          messageAction("saveFail");
        });
      },
    });
  };
  const onDeleteSub = (index:number) => {
    const tmp = subCategory
    tmp.splice(index,1)
    setSubCategory([...tmp]);
  };
  const onCloseDrawer = () => {
    setFormItem({ name: "", parent_id: NaN, is_active: true });
    setnewSub({ name: "", is_active: true });
    onCloseModal();
    setSubCategory([]);
  };

  const onHandleChange = (name: string, value: string | boolean) => {
    setFormItem({ ...formItem, [name]: value });
  };
  const columns: ColumnsType<ISubCategory> = [
    {
      title: "ชื่อรายการย่อย",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "สถานะ",
      dataIndex: "is_active",
      key: "is_active",
      render: (value: boolean) => (
        <TagStatus status={value ? "active" : "inactive"} />
      ),
    },
    {
      title: "ลบ",
      key: "action",
      align: "center",
      fixed: "right",
      width: 90,
      render: (_: any, item: ISubCategory,index:number) => (
        <ButtonCustom
          shape="circle"
          type="dashed"
          icon="delete"
          onClick={() => onDeleteSub(index)}
        />
      ),
    },
  ];

  return (
    <div>
      <ModalFormStyled
        width={500}
        title={"เพิ่มประเภทสินค้า"}
        visible={visible === "main"}
        onCancel={onCloseModal}
        footer={
          <ButtonCustom
            key="submit"
            onClick={onSubmitData}
            type="primary"
            icon="add"
            text="บันทึก"
          />
        }
      >
        <Spin spinning={loading} tip="กำลังบันทึก...">
          <Row gutter={[8, 8]} align="bottom">
            <Col xl={24}>
              <Text underline>รายการหลัก</Text>
            </Col>
            <Col xl={18}>
              <Input
                placeholder={`ชื่อประเภทสินค้าหลัก  (ความยาวไม่เกิน 50 ตัวอักษร)`}
                value={formItem.name}
                style={{ width: "100%" }}
                onChange={(e: any) => onHandleChange("name", e.target.value)}
              />
            </Col>
            <Col span={4} style={{ textAlign: "end" }}>
              <div style={{ marginBottom: "3px" }}>
                <Text>เปิดใช้งาน</Text>
              </div>
              <Switch
                checked={formItem.is_active}
                onChange={(e: any) => onHandleChange("is_active", e)}
              />
            </Col>
            <Col xl={24}>
              <Text underline>รายการย่อย</Text>
            </Col>
            <Col xl={18}>
              <Input
                placeholder={`ชื่อประเภทสินค้าย่อย  (ความยาวไม่เกิน 50 ตัวอักษร)`}
                value={newSub.name}
                suffix={<EditFilled />}
                style={{ width: "100%" }}
                onChange={(e: any) =>
                  setnewSub({ ...newSub, name: e.target.value })
                }
              />
            </Col>
            <Col span={4} style={{ textAlign: "end" }}>
              <div style={{ marginBottom: "3px" }}>
                <Text>เปิดใช้งาน</Text>
              </div>
              <Switch
                checked={newSub.is_active}
                onChange={(e: any) => setnewSub({ ...newSub, is_active: e })}
              />
            </Col>
            <Col span={2} style={{ textAlign: "end" }}>
              <PlusCircleOutlined
                onClick={onUpdateSub}
                style={{ fontSize: "20px" }}
              />
            </Col>
          </Row>
          <div style={{ marginTop: "10px" }}>
            <div style={{ marginBottom: "5px" }}>
              <Text underline>รายการที่ถูกเพิ่ม</Text>
            </div>
            {subCategory.length === 0 ? (
              <Col xl={24}>
                <Empty
                  description="ยังไม่มีรายการย่อยในประเภทนี้"
                  style={{ margin: "15px 0" }}
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                />
              </Col>
            ) : (
              <Table columns={columns} dataSource={subCategory} />
            )}
          </div>
        </Spin>
      </ModalFormStyled>
    </div>
  );
}
