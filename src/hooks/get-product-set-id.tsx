import { AxiosResponse } from "axios";
import { IFormProductSet } from "src/interfaces/interface-form";
import { AXIOS, API } from "src/services";

const USE_HOOK_PRODUCT_SET_ID = async (id: string | undefined) => {
  try {
    let { data }: AxiosResponse<any, any> = await AXIOS({
      method: "get",
      url: API.PRODUCTS_SET_ID,
      params: {
        product_set_id: id,
      },
    });
    return data.data as IFormProductSet
    // Work with the response...
  } catch (err: any) {
    // Handle error
    return {} as IFormProductSet
  }
};

export default USE_HOOK_PRODUCT_SET_ID;
