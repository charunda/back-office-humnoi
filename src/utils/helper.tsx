import React from "react";
import { Tag } from "antd";
import { srcPlacholder } from "src/mock/mock-img-src";
import { IMAGE } from "src/services/host";

export const renderImage = (imagepath: string) => {
  const url = imagepath ?? "";
  return url ? `${IMAGE}${imagepath}` : srcPlacholder;
};

export const renderStatusTag = (statusId: number, statusName: string) => {
  switch (statusId) {
    case 1:
      return <Tag color="volcano">{statusName}</Tag>;
    case 2:
      return <Tag color="orange">{statusName}</Tag>;
    case 3:
      return <Tag color="blue">{statusName}</Tag>;
    case 4:
      return <Tag color="gold">{statusName}</Tag>;
    case 5:
      return <Tag color="magenta">{statusName}</Tag>;
    case 6:
      return <Tag color="green">{statusName}</Tag>;
    case 7:
      return <Tag color="red">{statusName}</Tag>;
    default:
      break;
  }
};

export const onKeyDownOnlyNumber = (e: any) => {
  const key = e.key;
  const code = e.code;
  if (
    isNaN(parseInt(key)) === true &&
    key !== "Backspace" &&
    key !== "Tab" &&
    code !== "KeyV"
  ) {
    e.preventDefault();
  }
};
export const onKeyDownPrice = (event: any) => {
  const key = event.key;
  if (
    isNaN(parseInt(key)) === true &&
    key !== "Backspace" &&
    key !== "." &&
    key !== "Tab"
  ) {
    event.preventDefault();
  }
};
