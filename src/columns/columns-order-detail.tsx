import React from "react";
import type { ColumnsType } from "antd/lib/table";
import { Avatar, Typography } from "antd";
import { renderImage } from "src/utils/helper";
import { IResOpl } from "src/interfaces/interface-order";
import { convertToPrice } from "src/utils/pattern-number";

const { Text } = Typography;
const ColumnsProductItemOrder: ColumnsType<IResOpl> = [
  {
    title: "ลำดับ",
    dataIndex: "index",
    key: "index",
    width: 10,
    // fixed: true,
    align: "center",
    render: (_value, _item, index) => <span>{index + 1}</span>,
  },
  {
    title: "รูปสินค้า",
    dataIndex: "image_file",
    key: "image_file",
    align: "center",
    width: 30,
    render: (value: any) => (
      <Avatar shape="square" size={48} src={renderImage(value)} />
    ),
  },
  {
    title: "ชือสินค้า",
    dataIndex: "name",
    key: "name",
    width: 80,
  },
  {
    title: "ราคาต่อหน่วย(บาท)",
    dataIndex: "price_show",
    key: "price_show",
    width: 80,
    align: "right",
    render: (value: number) => <Text>{convertToPrice(value)}</Text>,
  },
  {
    title: "ราคารวม(บาท)",
    dataIndex: "net_price",
    key: "net_price",
    width: 80,
    align: "right",
    render: (value: number) => <Text strong>{convertToPrice(value)}</Text>,
  },
];

export default ColumnsProductItemOrder;
