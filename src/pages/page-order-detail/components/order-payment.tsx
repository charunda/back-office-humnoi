import { Col, Row, Typography } from "antd";
import dayjs from "dayjs";
import React from "react";
import { ButtonCustom } from "src/components";
import { IOrderDetail } from "src/interfaces/interface-order";
import { renderImage, renderStatusTag } from "src/utils/helper";
import { convertToPrice } from "src/utils/pattern-number";
import {
  DivRoot,
  ImageSlip,
  Text14,
  TextParagraph,
} from "./styled-component-page";
const { Text } = Typography;
interface IProps {
  propsOrder: IOrderDetail;
  onApprovePayment: (is_approve: boolean) => void;
}
export default function OrderPayment({ propsOrder, onApprovePayment }: IProps) {
  const {
    status,
    addresses,
    total_net_price,
    payment_slip_image,
    payment_date,
    status_id,
  } = propsOrder;
  // const checkDisabledPayment = () => {
  //   if (status_id === 2) {
  //     return false;
  //   } else {
  //     return true;
  //   }
  // };
  return (
    <DivRoot>
      <Row justify="space-between">
        <Col xl={18}>
          <Text14 strong>การชำระเงิน</Text14>
        </Col>
        <Col xl={6}>{renderStatusTag(status_id, status)} </Col>
        <Col xl={24}>
          <Text>{`วิธีการชำระเงิน  :  `}</Text>
        </Col>
      </Row>
      <Row style={{ marginTop: "1rem" }}>
        <Col xl={16}>
          <Text14 strong>รายละเอียดการชำระเงิน</Text14>
          <TextParagraph
            style={{ marginTop: "10px" }}
          >{`ชื่อลูกค้า : ${addresses.first_name} ${addresses.last_name}`}</TextParagraph>
          <Text>{`วันเวลาที่ชำระ : ${
            payment_date
              ? dayjs(payment_date).format("DD/MM/YYYY")
              : ""
          }`}</Text>
          <TextParagraph>
            {`จำนวนที่ชำระ : `}
            <Text strong>{convertToPrice(total_net_price)}</Text>
          </TextParagraph>
        </Col>
        <Col xl={8}>
          <ImageSlip alt="" src={renderImage(payment_slip_image)}></ImageSlip>
        </Col>
      </Row>
      <Row
        hidden={status_id > 2}
        justify="center"
        style={{ marginTop: "1rem" }}
        gutter={[16, 16]}
      >
        <Col xl={12}>
          <ButtonCustom
            danger
            width="100%"
            onClick={() => onApprovePayment(false)}
            type="default"
            text="ยกเลิกการชำระเงิน"
          />
        </Col>
        <Col xl={12}>
          <ButtonCustom
            onClick={() => onApprovePayment(true)}
            htmlType="submit"
            width="100%"
            type="primary"
            text="ยืนยันการชำระเงินถูกต้อง"
          />
        </Col>
      </Row>
    </DivRoot>
  );
}
