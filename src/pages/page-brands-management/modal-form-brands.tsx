import React, {  useEffect, useContext } from "react";
import { Col, Form, Input, Row } from "antd";
import { ButtonCustom, FormUploadCoverImage } from "src/components";
import { ModalFormStyled } from "src/styles/modal-styled";
import { GlobalContext } from "src/contexts/store/context-reducer";
import { RcFile } from "antd/lib/upload";
import { IFormCoverImage } from "src/interfaces/interface-form";
import { defaultCoverImage } from "src/data/data-initial-value";

interface IForm {
  name: string;
  image: File;
}
interface IPropsModalCaergory {
  isModalOpen: boolean;
  onCloseModal: () => void;
  onAddNewData: (data: IForm) => void;
  onEditData: (data: IForm) => void;
  item: { name: string; image_file: any; id: null | number };
}

export default function ModalFormBrands({
  isModalOpen,
  onCloseModal,
  item,
  onAddNewData,
  onEditData,
}: IPropsModalCaergory) {
  const {
    coverImage,
    onSetToStore,
  }: { coverImage: IFormCoverImage; onSetToStore: any } =
    useContext(GlobalContext);

  const [form] = Form.useForm();

  const onFinished = () => {
    const data = form.getFieldsValue();
    const updateData = {
      ...data,
      image: coverImage.base64 ? coverImage.file : coverImage.imagePath,
    };
    if (item.id) {
      onEditData(updateData);
    } else {
      onAddNewData(updateData);
    }
  };

  const onReset = () => {
    form.resetFields();
    onSetToStore({
      name: "coverImage",
      value: {
        ...defaultCoverImage,
      } as IFormCoverImage,
    });
    onCloseModal();
  };
  useEffect(() => {
    if (item.id) {
      form.setFieldsValue({
        name: item.name,
        id: item.id,
        image: item.image_file,
      });
      onSetToStore({
        name: "coverImage",
        value: {
          imagePath: item.image_file,
          file: {} as RcFile,
          base64: "",
          product_list_id: NaN,
        } as IFormCoverImage,
      });
    }
  }, [item.id]);
  return (
    <div>
      <ModalFormStyled
        width={450}
        title="จัดการแบรนด์สินค้า"
        visible={isModalOpen}
        onCancel={onReset}
        footer={
          <ButtonCustom
            type="primary"
            icon="save"
            text="บันทึกการเปลี่ยนแปลง"
            onClick={onFinished}
          />
        }
      >
        <Form layout="vertical" form={form}>
          <Row gutter={[16, 4]} justify="center">
            <Col xl={24}>
              <Form.Item name="name" required label="ชื่อแบรนด์">
                <Input size="large" />
              </Form.Item>
            </Col>
            <Col xl={24}>
              <Form.Item name="image" label="รูปภาพหน้าปก">
                <FormUploadCoverImage />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </ModalFormStyled>
    </div>
  );
}
