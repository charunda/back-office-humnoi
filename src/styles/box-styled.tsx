import styled from "styled-components";

export const BoxActionMainTable = styled.div`
  margin-top: 2rem;
  margin-bottom: 1rem;
`;

export const BoxFlexSpaceAround = styled.div`
 display: flex;
 align-items: center;
 justify-content: space-around;
`;

