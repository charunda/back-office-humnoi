import { AxiosResponse } from "axios";
import { AXIOS, API } from "src/services";

const USE_HOOK_SUB_CATEGORY = async (parentId:string) => {
  try {
    let { data }: AxiosResponse<any, any> = await AXIOS({
      method: "get",
      url: API.SUB_CATEGORY_ALL,
      params: {parent_id: parentId}
    });
    return data.data;
    // Work with the response...
  } catch (err: any) {
    // Handle error
    return [];
  }
};

export default USE_HOOK_SUB_CATEGORY;
