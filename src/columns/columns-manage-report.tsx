import React from "react";
import type { ColumnsType } from "antd/lib/table";
import { Avatar, Typography } from "antd";
import { convertToPrice } from "src/utils/pattern-number";
const { Text } = Typography;

const ColumnsManageReport: ColumnsType<any> = [
  {
    title: "ลำดับ",
    dataIndex: "index",
    key: "index",
    fixed: true,
    align: "center",
    width: 50,
    render: (_value, _item, index) => <span>{index + 1}</span>,
  },
  {
    title: "รูปสินค้า",
    dataIndex: "product_image",
    key: "product_image",
    align: "center",
    fixed: true,
    width: 60,
    render: (_, item) => (
      <Avatar shape="square" size={48} src={item.product_image} />
    ),
  },
  {
    title: "ชื่อสินค้า",
    dataIndex: "product_name",
    key: "product_name",
    fixed: true,
  },
  {
    title: "ประเภท",
    dataIndex: "product_type",
    key: "product_type",
  },
  {
    title: "จำนวนยอดขาย (ชิ้น)",
    dataIndex: "total_amount",
    key: "total_amount",
    align: "center",
  },
  {
    title: "ราคาขาย (บาท/ขิ้น)",
    dataIndex: "price",
    key: "price",
    align: "center",
    render: (value: number) => {
      return <span>{convertToPrice(value)}</span>;
    },
  },
  {
    title: "รวมยอดขาย (บาท)",
    dataIndex: "total",
    key: "total",
    align: "center",
    render: (value: number) => {
      return <span>{convertToPrice(value)}</span>;
    },
  },
  {
    title: "ต้นทุน (บาท/ขิ้น)",
    dataIndex: "cost",
    key: "cost",
    align: "center",
    render: (value: number) => {
      return <span>{convertToPrice(value)}</span>;
    },
  },
  {
    title: "รวมต้นทุน (บาท)",
    dataIndex: "total_cost",
    key: "stototal_costck",
    align: "center",
    render: (value: number) => {
      return <span>{convertToPrice(value)}</span>;
    },
  },

  {
    title: "กำไร (บาท/ขิ้น)",
    dataIndex: "profit",
    key: "profit",
    align: "center",
    render: (value: number) => {
      return <span>{convertToPrice(value)}</span>;
    },
  },
  {
    title: "รวมกำไร (บาท)",
    dataIndex: "total_profit",
    key: "total_profit",
    align: "center",
    render: (value: number) => {
      return <Text strong type='success'>{convertToPrice(value)}</Text>;
    },
  },
];

export default ColumnsManageReport;
