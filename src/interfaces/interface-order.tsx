export interface IProductImage {
  id: number;
  path: string;
  product_list_id: number;
}
export interface IShipment {
  id: number;
  name: string;
  price: number;
  price_show: number;
  image_file: string;
}
export interface IStoreBank {
  id: number;
  name: string;
  branch: string;
  account_number: string;
  owner_name: string;
  image_file: string;
}
export interface IAddress {
  id: number;
  first_name: string;
  last_name: string;
  phone: string;
  address: string;
  sub_district: string;
  district: string;
  province: string;
  postcode: string;
  customer_id: number;
}
export interface IProductList {
  id: number;
  name: string;
  price: number;
  discount: number;
  price_show: number;
  qty: number;
  product_list_images: IProductImage;
  stock: number;
  free_shipping: true;
  sold: number;
  product_id: number;
  detail: string;
  color: string;
  battery: number;
  product_set_id: number;
  product_list_type_id: number;
}
export interface IResOpl {
  product_type: string;
  product_list_id: number;
  product_set_id: number;
  price_per_unit: number;
  name: string;
  price: number;
  price_show: number;
  net_price: number;
  discount: number;
  total_discount: number;
  amount: number;
  image_file: string;
  stock: number;
  color: string;
  free_cotton: false;
  free_liquid_by_store: string;
  free_shipping: true;
  product_set: {
    box: {
      product_list: IProductList;
    };
    atom: {
      atom_modify: false;
      product_list: IProductList;
    };
    charger: {
      product_list: IProductList;
    };
    cotton: {
      product_list: IProductList;
    };
    battery: {
      product_list: IProductList;
    };
    pod: {
      product_list: IProductList;
    };
    mod: {
      product_list: IProductList;
    };
  };
  product_lists: IProductList[];
}
export interface IResOplAmount {
  product_id: number;
  product_list_id: number;
  name: string;
  detail: string;
  amount: number;
  price: number;
  image_file: string;
}
export interface IOrderDetail {
  id: number;
  code: string;
  status: string;
  status_id: number;
  order_date: Date;
  payment_date: Date;
  payment_slip_date: Date;
  total_price: number;
  total_discount: number;
  total_net_price: number;
  total_shipping_price: number;
  total_discount_shipping_price: number;
  pay_amount: number;
  payment_slip_image: string;
  customer_id: number;
  customer_name: string;
  bank_id: number;
  shipment_id: number;
  address_id: number;
  orders_product_lists: [{}];
  shipments: IShipment;
  addresses: IAddress;
  store_banks: IStoreBank;
  res_opl: IResOpl[];
  opl_amount: IResOplAmount[];
  shipments_date: Date;
  tracking_code: string;
}

export interface IConfirmShipment {
  tracking_number: string;
  shipping_date: string;
  order_id: number;
}
