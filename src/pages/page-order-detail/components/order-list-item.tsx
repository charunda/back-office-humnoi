import React from "react";
import { Col, Row, Table, Typography } from "antd";
import {
  DivBorder,
  DivDarkGrey,
  DivRoot,
  Text14,
} from "./styled-component-page";
import { IOrderDetail } from "src/interfaces/interface-order";
import dayjs from "dayjs";
import { ColumnsProductItemOrder } from "src/columns";
import { convertToPrice } from "src/utils/pattern-number";
const { Text } = Typography;
interface IProps {
  propsOrder: IOrderDetail;
}
export default function OrderListItem({ propsOrder }: IProps) {
  const {
    res_opl,
    addresses,
    total_price,
    total_shipping_price,
    total_net_price,
  } = propsOrder;
  return (
    <DivRoot>
      <DivBorder>
        <Row gutter={[8, 2]}>
          <Col xl={14}>
            <Text14 strong>รายการคำสั่งซื้อ</Text14>
          </Col>
          <Col xl={5} style={{ textAlign: "end" }}>
            <Text>หมายเลขคำสั่งซื้อ : </Text>
          </Col>
          <Col xl={5}>
            <Text strong>{propsOrder.code}</Text>
          </Col>
          <Col xl={14}>
            <Text>{`สั่งซื้อเมื่อ : ${dayjs(propsOrder.order_date).format(
              "DD/MM/YYYY"
            )} `}</Text>
          </Col>
          <Col xl={5} style={{ textAlign: "end" }}>
            <Text>{`ชื่อผู้สั่งซื้อ :`}</Text>
          </Col>
          <Col xl={5}>
            <Text>{`${addresses.first_name} ${addresses.last_name}`}</Text>
          </Col>
        </Row>
      </DivBorder>
      <div style={{ marginTop: "1rem", height: "23rem",overflow: 'auto' }}>
        <Table
          pagination={false}
          columns={ColumnsProductItemOrder}
          dataSource={res_opl}
        />
      </div>
      <DivDarkGrey style={{ marginTop: "1rem" }}>
        <Row align="middle">
          <Col xl={17}>
            <Row>
              <Col xl={8}>
                <Text type="secondary">ยอดรวม</Text>
              </Col>
              <Col xl={10}>
                <Text type="secondary">{convertToPrice(total_price)}</Text>
              </Col>
            </Row>
            <Row>
              <Col xl={8}>
                <Text type="secondary">ค่าจัดส่ง</Text>
              </Col>
              <Col xl={10}>
                <Text type="secondary">{convertToPrice(total_shipping_price)}</Text>
              </Col>
            </Row>
          </Col>
          <Col xl={7}>
            <Row>
              <Col xl={15}>
                <Text strong>รวมทั้งสิ้น</Text>
              </Col>
              <Col xl={9}>
                <Text strong>{convertToPrice(total_net_price)}</Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </DivDarkGrey>
    </DivRoot>
  );
}
