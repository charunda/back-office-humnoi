import React from "react";
import "./App.css";
import { SideNavbar } from "src/containers";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import {
  PageSaltnic,
  PageFreebase,
  PageOrder,
  PaeCategory,
  PageMenageProduct,
  PageSingleProduct,
  PageSetProduct,
  PageBrands,
  PageManagePermission,
  PageReportManagement,
  PageBannersManagement,
  PageLogin,
  PageNormalProduct,
  PageOrderDetail,
} from "src/pages";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageLogin />} />
          <Route
            path="/admin"
            element={
              <PrivateWrapper>
                <SideNavbar />
              </PrivateWrapper>
            }
          >
            <Route index element={<PageOrder />} />
            <Route path="/admin/banner" element={<PageBannersManagement />} />
            <Route path="/admin/categories" element={<PaeCategory />} />
            <Route path="/admin/brands" element={<PageBrands />} />
            <Route path="/admin/product/set" element={<PageMenageProduct />} />
            <Route
              path="/admin/product/:name/:id"
              element={<PageMenageProduct />}
            />
            <Route
              path="/admin/product/detail/:code"
              element={<PageOrderDetail />}
            />
            <Route
              path="/admin/single-product/:parentId/:name"
              element={<PageSingleProduct />}
            />
            <Route
              path="/admin/single-product/product-id/:productId/parent/:parentId/category/:catId/:name"
              element={<PageSingleProduct />}
            />
            <Route path="/admin/set-product" element={<PageSetProduct />} />
            <Route
              path="/admin/set-product/product-set-id/:id"
              element={<PageSetProduct />}
            />
            <Route
              path="/admin/freebase-product/:parentId"
              element={<PageFreebase />}
            />
            <Route
              path="/admin/freebase-product/product-id/:productId/parent/:parentId/category/:catId"
              element={<PageFreebase />}
            />
            <Route
              path="/admin/saltnic-product/:parentId"
              element={<PageSaltnic />}
            />
            <Route
              path="/admin/saltnic-product/product-id/:productId/parent/:parentId/category/:catId"
              element={<PageSaltnic />}
            />
            <Route
              path="/admin/regular-product/:parentId/:name"
              element={<PageNormalProduct />}
            />
            <Route
              path="/admin/regular-product/product-id/:productId/parent/:parentId/category/:catId/:name"
              element={<PageNormalProduct />}
            />
            <Route
              path="/admin/permission-management"
              element={
                <PrivateOwner>
                  <PageManagePermission />
                </PrivateOwner>
              }
            />
            <Route
              path="/admin/reports-management"
              element={
                <PrivateOwner>
                  <PageReportManagement />
                </PrivateOwner>
              }
            />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
const PrivateWrapper = ({ children }: { children: JSX.Element }) => {
  const auth = localStorage.getItem("access_token");
  return auth ? children : <Navigate to="/" replace />;
};

const PrivateOwner = ({ children }: { children: JSX.Element }) => {
  const auth = localStorage.getItem("name");
  return auth === "owner" ? children : <Navigate to="/" replace />;
};
