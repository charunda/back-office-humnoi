import { Button } from "antd";
import React from "react";
import styled from "styled-components";

interface IPropsCardAction {
  onClick: () => void;
  type: "delete" | "edit" | "list";
  disabled?: false | true;
  text: string;
}
const ButtonDiv = styled(Button)`
  display: flex;
  justify-content: center;
  align-item: center;
  cursor: pointer;
  border: 1px solid;
  border-radius: 5px;
  padding: 5px;
  width: 100%;
`;
const TextButton = styled.span`
  font-size: 12px;
`;

export default function CardAction({
  onClick,
  type,
  text,
  disabled,
}: IPropsCardAction) {
  return (
    <ButtonDiv
      onClick={onClick}
      disabled={disabled}
      style={{
        borderColor:
        disabled ? 'transparent' :
          type === "delete"
            ? "#FED695"
            : type === "list"
            ? "#D3ACF7"
            : "#91D5FF",
      }}
    >
      <TextButton
        style={{
          color:
          disabled ? '#d9d9d9' :
            type === "delete"
              ? "#D36B08"
              : type === "list"
              ? "#531CAA"
              : "#0A6CD9",
        }}
      >
        {text}
      </TextButton>
    </ButtonDiv>
  );
}
