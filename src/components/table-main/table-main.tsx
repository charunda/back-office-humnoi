import { OrderedListOutlined } from "@ant-design/icons";
import { Table, Tag } from "antd";
import React from "react";
import { IExpandDataCustom } from "src/interfaces/interface-table";
import styled from "styled-components";

interface IPropsTable {
  pagination?: any;
  loading?: boolean;
  columns: any;
  dataSource: object[];
  scroll?: { x?: number; y?: number };
  dataSourceExpand?: IExpandDataCustom[];
}

const TableStyled = styled(Table)`
  padding: 0px 10px 0px 10px;
`;
const TagTitleTable = styled(Tag)`
  border-radius: 0px;
`;
export default function TableMain(props: IPropsTable) {
  const expandedRowRender = () => {
    return (
      <Table
        bordered
        title={() => (
          <div>
            <TagTitleTable icon={<OrderedListOutlined />} color={"geekblue"}>
              รายชื่อสีของสินค้า
            </TagTitleTable>
          </div>
        )}
        size="small"
        dataSource={props.dataSourceExpand}
        pagination={false}
      />
    );
  };
  return (
    <TableStyled
      expandable={{
        expandedRowRender,
        showExpandColumn:
          props.dataSourceExpand === undefined ||
          props.dataSourceExpand.length === 0
            ? false
            : true,
      }}
      sticky
      {...props}
      size="small"
    />
  );
}
