import {
  srcPod,
  srcBox,
  srcFreebase,
  srcMod,
  srcSaltnic,
  srcStoreLiquid,
  srcAtom,
  srcCharger,
  srcCoil,
  srcWire,
  srcAccessery,
  srcProductSet,
  srcColorBox,
} from "src/mock/mock-img-src";
import { IExpandDataCustom, IMockData } from "src/interfaces/interface-table";

export const mockDataProductCustom: IExpandDataCustom[] = [
  {
    key: "0",
    price: 200,
    stock: 59,
    product_color: "สี Witch Skull",
    product_image: srcColorBox,
  },
  {
    key: "1",
    price: 200,
    stock: 45,
    product_color: "สี Black",
    product_image: srcColorBox,
  },
  {
    key: "2",
    price: 200,
    stock: 45,
    product_color: "สี Gun Meta",
    product_image: srcColorBox,
  },
];
export const mockDataProductSet: IMockData = {
  key: "0",
  product_name: "Set พร้อมสูบ Voopoo Argus pod 20w (เซ็ตพร้อมสูบ มาโบซอล)",
  product_cat: "เซ็ตพร้อมสูบ",
  price: 2200,
  stock: 18,
  src: srcProductSet,
};
export const mockDataBox: IMockData = {
  key: "0",
  product_name: "DOVPO M VV Box MOD",
  product_cat: "กล่อง",
  price: 2200,
  stock: 18,
  src: srcBox,
};
export const mockDataPod: IMockData = {
  key: "0",
  product_name: "SMOK - Nfix Pro 25W Pod System Kit",
  product_cat: "พอต",
  price: 1890,
  stock: 18,
  src: srcPod,
};
export const mockDataMod: IMockData = {
  key: "0",
  product_name: "Morph 219W Vape Ecig Modt",
  product_cat: "มอต",
  price: 590,
  stock: 18,
  src: srcMod,
};
export const mockDataFreebase: IMockData = {
  key: "0",
  product_name: " Sing5 Freebase 60ml",
  product_cat: "น้ำยา Freebase",
  price: 200,
  stock: 18,
  src: srcFreebase,
};
export const mockDataSaltnic: IMockData = {
  key: "0",
  product_name: "Sengoku Saltnic 30ml",
  product_cat: "น้ำยา Saltnic",
  price: 4500,
  stock: 18,
  src: srcSaltnic,
};
export const mockDataStoreLiquid: IMockData = {
  key: "0",
  product_name: "SMOK - Nfix Pro 25W Pod System Kit",
  product_cat: "น้ำยา Freebase",
  price: 2200,
  stock: 18,
  src: srcStoreLiquid,
};
export const mockDataAtom: IMockData = {
  key: "0",
  product_name: "อะตอมแท้ Apocalypse 25mm RDA",
  product_cat: "อะตอม",
  price: 2200,
  stock: 18,
  src: srcAtom,
};
export const mockDataCoil: IMockData = {
  key: "0",
  product_name: "Coil Vap X Geyser",
  product_cat: "คอยล์",
  price: 2200,
  stock: 18,
  src: srcCoil,
};
export const mockDataBattery: IMockData = {
  key: "0",
  product_name: "Vape69TH รางชาร์จ Vapcell Q1",
  product_cat: "รางชาร์ท",
  price: 2200,
  stock: 18,
  src: srcCharger,
};
export const mockDataWire: IMockData = {
  key: "0",
  product_name: "ลวดสำเร็จNI 80 UD 0.5 จำนวน 6 คุ่",
  product_cat: "ลวด",
  price: 25,
  stock: 18,
  src: srcWire,
};

export const mockDataAccessery: IMockData = {
  key: "0",
  product_name: "ลวดสำเร็จNI 80 UD 0.5 จำนวน 6 คุ่",
  product_cat: "ลวด",
  price: 25,
  stock: 18,
  src: srcAccessery,
};
