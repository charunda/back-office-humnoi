import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Typography,
  Input,
  Card,
  Spin,
  Modal,
  Avatar,
  Skeleton,
} from "antd";
import {
  HeaderPage,
  ButtonCustom,
  BoxStyled,
  CardAction,
  TagStatus,
} from "src/components";
import styled from "styled-components";
import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";
import {
  ICategory,
  ITableProduct,
  ITableUser,
} from "src/interfaces/interface-response";
import { messageAction } from "src/utils/message";
import { propsModalDelete } from "src/utils/props-component";
import ModalFormPermission from "./modal-form-permission";
import { UserOutlined } from "@ant-design/icons";
import { IFormUsers, IPropsModalUser } from "src/interfaces/interface-form";
import { formUser } from "src/data/data-initial-value";

const { confirm } = Modal;
const { Title } = Typography;
export default function PageOrderManagement() {
  const [dataRows, setDataRows] = useState<ITableUser>({
    total_rows: 0,
    rows: [],
    limit: 10,
    page: 1,
    search: "",
  });
  const [modalProps, setModalProps] = useState<IPropsModalUser>({
    visible: false,
    defaultData: formUser,
    type: "add",
  });
  const [loading, setLoading] = useState(true);

  const onAddItem = () => {
    setModalProps({
      defaultData: formUser,
      visible: true,
      type: "add",
    });
  };

  const onFetchData = (search?: string) => {
    setLoading(true);
    AXIOS({
      method: "get",
      url: API.USERS,
      params: {
        search: search ?? "",
        limit: dataRows.limit,
        page: dataRows.page,
      },
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: any = data.data;
        setDataRows({
          ...dataRows,
          rows: response.rows,
          total_rows: response.total_rows,
        });
        setLoading(false);
      })

      .catch((error: AxiosError<any, any>) => {
        messageAction("getError", error.message);
        setLoading(false);
      });
  };
  const onDeleteItem = (usrId: number) => {
    confirm({
      ...propsModalDelete,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          AXIOS({
            method: "delete",
            url: API.USERS,
            params: { user_id: usrId },
          })
            .then(() => {
              onFetchData();
              resolve();
              messageAction("deleteSuccess");
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          messageAction("deleteFail");
        });
      },
    });
  };

  useEffect(onFetchData, []);
  const onFinishedModal = () => {
    onCloseModal();
    onFetchData();
  };

  const onCloseModal = () => {
    setModalProps({
      defaultData: formUser,
      visible: false,
      type: "add",
    });
  };
  const onEditItem = (item: IFormUsers) => {
    setModalProps({
      visible: true,
      defaultData: item,
      type: "edit",
    });
  };

  return (
    <div>
      <HeaderPage
        header="การจำกัดสิทธิ์การใช้งาน"
        subHeader="สามารถจัดการสิทธิ์การใช้งาน / ลบ / แก้ไขสิทธิ์การใช้งานได้"
      />
      <div className="App-paper-page">
        <BoxStyled color="white">
          <Row align="middle">
            <Col xl={4}>
              <Title level={5}>รายชื่อผู้ใช้งานทั้งหมด</Title>
            </Col>
            <Col xl={15}>
              <Input.Search
                allowClear
                onSearch={(value: string) => {
                  onFetchData(value);
                }}
              />
            </Col>
            <Col offset={1} xl={2}>
              <ButtonCustom
                type="primary"
                icon="add"
                text="เพิ่มผู้ใช้งาน"
                onClick={onAddItem}
              />
            </Col>
          </Row>
        </BoxStyled>
        <Spin spinning={loading} tip="กำลังโหลดข้อมูล...">
          {loading ? (
            <Skeleton active paragraph={{ rows: 10 }}></Skeleton>
          ) : (
            <div style={{ marginTop: "1rem" }}>
              <Row gutter={[16, 16]} align="middle">
                {dataRows.rows.map((item: IFormUsers) => (
                  <Col xl={8} key={item.id}>
                    <CardStyled
                      actions={[
                        <DivAction>
                          <CardAction
                            text="แก้ไขผู้ใช้งาน"
                            onClick={() => onEditItem(item)}
                            type="edit"
                          />
                        </DivAction>,
                        <DivAction>
                          <CardAction
                            text="ลบผู้ใช้งาน"
                            onClick={() => onDeleteItem(item.id)}
                            type="delete"
                          />
                        </DivAction>,
                      ]}
                    >
                      <Row gutter={[13, 8]}>
                        <ColName xl={20}>
                          <div>
                            <TextBrand>ชื่อผู้ใช้งาน</TextBrand>
                            <SpanTitle>{item.username}</SpanTitle>
                          </div>
                        </ColName>
                        <Col xl={4}>
                          <Avatar
                            shape="circle"
                            size={48}
                            icon={<UserOutlined />}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <TextBrand>{item.phone}</TextBrand>
                      </Row>
                    </CardStyled>
                  </Col>
                ))}
              </Row>
            </div>
          )}
        </Spin>
        <ModalFormPermission
          modalProps={modalProps}
          onClose={onCloseModal}
          onFinished={onFinishedModal}
        />
      </div>
    </div>
  );
}
const ImageBrands = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 3px;
`;
const ColName = styled(Col)`
  display: flex;
  align-items: center;
`;
const SpanTitle = styled.span`
  font-size: 14px;
  font-weight: 500;
  margin-right: 1rem;
`;
const TextBrand = styled.p`
  margin-bottom: 0px;
  font-size: 12px;
  color: grey;
`;

const CardStyled = styled(Card)`
  width: 100%;
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  padding: 0px 7px 0px 7px;
  background: white;
  & .ant-card-body {
    padding: 10px 10px;
  }
`;
const DivAction = styled.div`
  display: flex;
  justify-content: center;
  align-item: center;
  padding: 0px 7px;
`;
