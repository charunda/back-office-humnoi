import React, { CSSProperties } from "react";
import styled from "styled-components";

const BoxStyled = styled.div`
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  background: ${(props) => (props.color === "white" ? "white" : "transparent")};
`;

interface IProps {
  type?: "div" | "table";
  color?: string;
  children: React.ReactNode;
  style?: CSSProperties
}
const BoxComponent = ({ type,color, style, children }: IProps) => {
  if (type === "table") {
    return (
      <BoxStyled color="white" style={{ marginTop: "1rem" }}>
        {children}
      </BoxStyled>
    );
  } else {
    return (
      <BoxStyled style={{ padding: "1.5rem",...style }} color={color}>
        {children}
      </BoxStyled>
    );
  }
};
export default BoxComponent;
