import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";

export default function IconDeleteUpload() {
  return (
      <FontAwesomeIcon icon={faTimesCircle} />
  );
}
//ยังไม่ได้ใช้