import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Input,
  Typography,
  // Checkbox,
  Tabs,
  Select,
  Statistic,
  DatePicker,
  Radio,
} from "antd";
import {
  HeaderPage,
  ButtonCustom,
  BoxStyled,
  TableMain,
  TitleTable,
} from "src/components";
import {
  BarChartOutlined,
  CreditCardOutlined,
  // DotChartOutlined,
  FilterFilled,
  LineChartOutlined,
  PieChartOutlined,
  // RadarChartOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { ColumnsManageOrder } from "src/columns";
import styled from "styled-components";
import { API, AXIOS } from "src/services";
import { ITableProduct } from "src/interfaces/interface-response";
import { AxiosError, AxiosResponse } from "axios";
import { useNavigate } from "react-router-dom";
import { orderStatus } from "src/data";
import { IRowsProduct } from "src/interfaces/interface-table";

const { Option } = Select;
const { Text } = Typography;
const { TabPane } = Tabs;

interface ITabsitem {
  name: string;
  value: string;
}
interface IStatistic {
  title: string;
  key: string;
  precision: number;
  suffix: string;
  prefix: any;
}
const statisItem: IStatistic[] = [
  {
    title: "รายการคำสั่งซื้อทั้งหมด",
    key: "order",
    precision: 0,
    suffix: "ชิ้น",
    prefix: <LineChartOutlined />,
  },
  {
    title: "รวมยอดขาย (บาท)",
    key: "total",
    precision: 2,
    suffix: "บาท",
    prefix: <PieChartOutlined />,
  },
];
const tabsItem: ITabsitem[] = [
  {
    name: "รายงานยอดขายรายวัน",
    value: "day",
  },
  {
    name: "รายงานยอดขายรายสัปดาห์",
    value: "week",
  },
  {
    name: "รายงานยอดขายรายเดือน",
    value: "month",
  },
  {
    name: "รายงานยอดขายรายปี",
    value: "year",
  },
];
const DivStat = styled.div`
  background: #f8f9fe;
  padding: 15px;
  border-radius: 5px;
  text-align: center;
  font-size: 14px;
`;
const StatStyled = styled(Statistic)`
  & .ant-statistic-title {
    font-size: 12px;
    color: #4d5fac;
  }
`;
const TabsStyle = styled(Tabs)`
  & .ant-tabs-tab {
    font-size: 12px;
  }
  & .ant-tabs-tab {
    color: #7b7d80;
    &.ant-tabs-tab-active {
      color: #5c68ff;
      font-weight: 500;
    }
  }
`;
interface IParams {
  page: number;
  limit: number;
  search: string;
  start: null | undefined;
  end: null | undefined;
  duration: "date" | "week" | "month" | "year";
}

interface IRowOrder {
  orders: IRowsProduct[];
  total_amount: number;
  total_orders: number;
}
interface IReportRow {
  total_rows: number;
  limit: number;
  page: number;
  search?: string;
  rows: IRowOrder;
}
export default function PageReportManagement() {
  const [loading, setLoading] = useState(true);
  const navigator = useNavigate();
  const [dateMomemt, setDateMoment] = useState({
    startDate: null,
    endDate: null,
  });
  const [dataRows, setDataRows] = useState<IReportRow>({
    total_rows: 0,
    rows: { orders: [], total_amount: 0, total_orders: 0 },
    limit: 10,
    page: 1,
  });
  const [params, setParams] = useState<IParams>({
    page: 1,
    limit: 10,
    search: "",
    start: undefined,
    end: undefined,
    duration: "date",
  });

  const onFetchData = () => {
    setLoading(true);
    const { start, end }: IParams = params;
    const updateParams: IParams = {
      ...params,
      start,
      end,
    };
    AXIOS({
      method: "get",
      url: API.REPORTS,
      params: updateParams,
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: IReportRow = data.data;
        setDataRows(response);
        setLoading(false);
      })

      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
      });
  };

  useEffect(onFetchData, [params.duration]);

  const onHandleChangeParams = (dataField: string, value: any) => {
    setParams({ ...params, [dataField]: value });
  };
  const onChangeTab = (value: any) => {
    setParams({
      page: 1,
      limit: 10,
      search: "",
      start: null,
      end: null,
      duration: value,
    });
    setDateMoment({ startDate: null, endDate: null });
  };
  return (
    <div>
      <HeaderPage
        header="การออกรายงาน  "
        subHeader="สามารถเรียกดูสรุปรายงานการขายสินค้าได้"
      />
      <div className="App-paper-page">
        <TabsStyle onChange={onChangeTab} defaultActiveKey="1" type="card">
          {tabsItem.map((tab: ITabsitem) => (
            <TabPane
              active={params.duration === tab.name}
              tab={tab.name}
              key={tab.value}
            >
              <Row gutter={[16, 8]}>
                {statisItem.map((state: IStatistic) => (
                  <Col span={12} key={state.key}>
                    <DivStat>
                      <StatStyled
                        value={
                          state.key === "order"
                            ? dataRows.rows.total_orders
                            : dataRows.rows.total_amount
                        }
                        {...state}
                        valueStyle={{
                          color: "#4D5FAC",
                          fontWeight: 600,
                          fontSize: 14,
                        }}
                      />
                    </DivStat>
                  </Col>
                ))}
              </Row>
            </TabPane>
          ))}
        </TabsStyle>
        <BoxStyled color="white" style={{ marginTop: "1rem" }}>
          <Row align="middle" gutter={[8, 8]}>
            <Col lg={12}>
              <Row gutter={[8, 8]} align="middle">
                <Col xl={11}>
                  <DatePicker
                    allowClear
                    placeholder="เลือกวันที่"
                    style={{ width: "100%" }}
                    onChange={(value: any, dateString: string) => {
                      onHandleChangeParams("start", dateString);
                      setDateMoment({ ...dateMomemt, startDate: value });
                    }}
                    value={dateMomemt.startDate}
                  />
                </Col>
                <Col xl={2} style={{ textAlign: "center" }}>
                  <Text>ถึง</Text>
                </Col>
                <Col xl={11}>
                  <DatePicker
                    allowClear
                    placeholder="เลือกวันที่"
                    style={{ width: "100%" }}
                    onChange={({ _d }: any, dateString: string) => {
                      onHandleChangeParams("end", dateString);
                      setDateMoment({ ...dateMomemt, endDate: _d });
                    }}
                    value={dateMomemt.endDate}
                  />
                </Col>
              </Row>
            </Col>

            <Col lg={8}>
              <Input
                value={params.search}
                allowClear
                placeholder="ค้นหาเลขที่ออเดอร์ / ชื่อผู้สั่งซื้อ"
                suffix={<SearchOutlined />}
                style={{ width: "100%" }}
                onChange={(e: any) =>
                  onHandleChangeParams("search", e.target.value)
                }
              />
            </Col>
            <Col lg={4}>
              <ButtonCustom
                width="100%"
                type="primary"
                icon="search"
                text="ค้นหาออเดอร์"
                onClick={onFetchData}
              />
            </Col>
          </Row>
        </BoxStyled>
        <BoxStyled color="white" type="table">
          <TitleTable title="รายงานยอดขายทั้งหมด" />
          <TableMain
            loading={loading}
            scroll={{ x: 1500 }}
            dataSource={dataRows.rows.orders}
            columns={[
              ...ColumnsManageOrder,
              {
                title: "รายละเอียดคำสั่งซื้อ",
                key: "action",
                width: 120,
                align: "center",
                fixed: "right",
                render: (_: any, { code }: any) => (
                  <ButtonCustom
                    type="default"
                    icon="detail"
                    text="รายละเอียด"
                    onClick={() => {
                      navigator(`/admin/product/detail/${code}`);
                    }}
                  />
                ),
              },
            ]}
          />
        </BoxStyled>
      </div>
    </div>
  );
}
