import React from "react";
import type { ColumnsType } from "antd/lib/table";
import { DataTypeOrder } from "src/interfaces/interface-table";
import { ButtonCustom } from "src/components";
import { Switch } from "antd";

const ColumnsModalCatergory: ColumnsType<DataTypeOrder> = [
  {
    title: "ลำดับ",
    dataIndex: "index",
    key: "index",
    width: 60,
    fixed: true,
    align: "center",
    render: (_value, _item, index) => <span>{index + 1}</span>,
  },
  {
    title: "ชื่อเมนูในประเภทนี้",
    dataIndex: "cat_name",
    key: "cat_name",
    width: 200,
  },
  {
    title: "สถานะ",
    key: "action",
    align: "center",
    width: 80,
    render: () => <Switch defaultChecked={true} />,
  },
  {
    title: "แก้ไขชื่อ",
    key: "action",
    align: "center",
    width: 80,
    render: () => <ButtonCustom type="text" text="แก้ไขชื่อ" />,
  },
  {
    title: "ลบ",
    key: "action",
    align: "center",
    width: 50,
    render: () => <ButtonCustom shape="circle" type="dashed" icon="delete" />,
  },
];

export default ColumnsModalCatergory;
