import { AxiosResponse } from "axios";
import { AXIOS, API } from "src/services";

const USE_HOOK_BRAND = async () => {
  try {
    let { data }: AxiosResponse<any, any> = await AXIOS({
      method: "get",
      url: API.BRNADS,
    });
    return data.data;
    // Work with the response...
  } catch (err: any) {
    // Handle error
    console.log(err);
    return [];
  }
};

export default USE_HOOK_BRAND;
