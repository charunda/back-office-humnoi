import React from "react";
import { createContext, Reducer, useReducer } from "react";
import {
  IFormCoverImage,
  IFormImageProduct,
  IFormProductSet,
} from "src/interfaces/interface-form";
import { ICategory } from "src/interfaces/interface-response";
import { RcFile } from "antd/lib/upload";
import { IRowsProduct } from "src/interfaces/interface-table";

const Contentext: any = {};
export const GlobalContext = createContext(Contentext);

interface IStateContext {
  categories: ICategory[];
  uploadedImage: IFormImageProduct[];
  coverImage: IFormCoverImage;
  selectedProduct: IRowsProduct[];
}

const initialState: IStateContext = {
  categories: [],
  uploadedImage: [],
  coverImage: {
    imagePath: "",
    base64: "",
    file: {} as RcFile,
    product_list_id: NaN
  },
  selectedProduct: [],
};

interface IAction {
  type: string;
  payload: any;
}

const contextReducer: Reducer<IStateContext, IAction> = (
  state: IStateContext,
  action: IAction
) => {
  const { type, payload } = action;

  switch (type) {
    case "SET_CATERGORY":
      return {
        ...state,
        categories: payload,
      };
    case "SET_FORM_UPLOADED":
      return {
        ...state,
        uploadedImage: payload,
      };
    case "SET_TO_STORE":
      const { name, value } = payload;
      return {
        ...state,
        [name]: value,
      };
    default:
      return state;
  }
};

export const GlobalProvider = ({ children }: { children: any }) => {
  const [golbalState, dispatchContext] = useReducer(
    contextReducer,
    initialState
  );

  const { categories, uploadedImage, coverImage, selectedProduct } =
    golbalState;
  const onSetCategory = (payload: ICategory) =>
    dispatchContext({ type: "SET_CATERGORY", payload });
  const onSetUploaded = (payload: any) =>
    dispatchContext({ type: "SET_FORM_UPLOADED", payload });
  const onSetToStore = (payload: any) =>
    dispatchContext({ type: "SET_TO_STORE", payload });
  return (
    <GlobalContext.Provider
      value={{
        onSetCategory,
        onSetUploaded,
        categories,
        onSetToStore,
        uploadedImage,
        coverImage,
        selectedProduct,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
