import React, { cloneElement } from "react";
import { GlobalProvider } from "./context-reducer";

function ContextComposer({ contexts, children } :{ contexts : any, children :any } ) {
  return contexts.reduce(
    (kids:any, parent:any) =>
      cloneElement(parent, {
        children: kids,
      }),
    children
  );
}
export default function ContextProvider({ children } : {children:any}) {
  return (
    <ContextComposer
      contexts={[<GlobalProvider children={{}} />]}
    >
      {children}
    </ContextComposer>
  );
}
