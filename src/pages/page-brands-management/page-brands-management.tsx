import React, { useState, useEffect } from "react";
import { Row, Col, Typography, Input, Card, Spin, Modal, Skeleton } from "antd";
import {
  HeaderPage,
  ButtonCustom,
  BoxStyled,
  CardAction,
} from "src/components";
import styled from "styled-components";
import { IBrands } from "src/interfaces/interface-response";
import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";
import { propsModalDelete, propsModalSave } from "src/utils/props-component";
import ModalFormBrands from "./modal-form-brands";
import { messageAction } from "src/utils/message";
import { renderImage } from "src/utils/helper";
const { confirm } = Modal;
const { Title } = Typography;

const defaultForm = {
  name: "",
  image_file: "",
  id: null,
};
export default function PageBrandsManagement() {
  const [modalForm, setModalForm] = useState({
    visible: false,
    item: defaultForm,
  });
  const [dataSource, setDataSource] = useState<IBrands[]>([]);
  const [loading, setLoading] = useState(false);

  const onEditItem = (item: any) => {
    setModalForm({ visible: true, item });
  };

  const onFetchData = (search?: string) => {
    setLoading(true);
    AXIOS({
      method: "get",
      url: API.BRNADS,
      params: { name: search ?? "" },
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        setDataSource(data.data);
        setLoading(false);
      })
      .catch((error: AxiosError<any, any>) => {
        setLoading(false);
      });
  };

  const onDeleteItem = (id: number) => {
    confirm({
      ...propsModalDelete,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          AXIOS({
            method: "delete",
            url: API.BRNADS,
            params: { id },
          })
            .then(() => {
              onFetchData();
              resolve();
              messageAction("deleteSuccess");
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          messageAction("deleteFail");
        });
      },
    });
  };
  useEffect(onFetchData, []);

  const onAddNewData = (data: any) => {
    const formData = new FormData();
    formData.append("name", data.name);
    formData.append("image", data.image);
    confirm({
      ...propsModalSave,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          AXIOS({
            method: "post",
            url: API.BRNADS,
            data: formData,
          })
            .then(() => {
              messageAction("saveSuccess");
              onCloseModal();
              onFetchData();
              resolve();
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          messageAction("saveFail");
          onCloseModal();
        });
      },
    });
  };
  const onEditData = (data: any) => {
    const formData = new FormData();
    formData.append("id", data.id);
    formData.append("name", data.name);
    if (data.image) {
      formData.append("image", data.image);
    }
    confirm({
      ...propsModalSave,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          AXIOS({
            method: "post",
            url: API.BRNADS,
            data: formData,
          })
            .then(() => {
              messageAction("saveSuccess");
              onCloseModal();
              onFetchData();
              resolve();
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          messageAction("saveFail");
          onCloseModal();
        });
      },
    });
  };
  const onCloseModal = () => {
    setModalForm({ visible: false, item: defaultForm });
  };

  return (
    <div>
      <ModalFormBrands
        isModalOpen={modalForm.visible}
        item={modalForm.item}
        onCloseModal={onCloseModal}
        onAddNewData={onAddNewData}
        onEditData={onEditData}
      />
      <HeaderPage
        header="จัดการแบรนด์สินค้า "
        subHeader="สามารถเพิ่ม/ลบ/แก้ไข แบรนด์สินค้าได้"
      />
      <div className="App-paper-page">
        <BoxStyled color="white">
          <Row align="middle">
            <Col xl={4}>
              <Title level={5}>แบรนด์สินค้าทั้งหมด</Title>
            </Col>
            <Col xl={15}>
              <Input.Search
                allowClear
                onSearch={(value: string) => {
                  onFetchData(value);
                }}
              />
            </Col>
            <Col offset={1} xl={2}>
              <ButtonCustom
                type="primary"
                icon="add"
                text="เพิ่มแบรนด์สินค้าใหม่"
                onClick={() =>
                  setModalForm({ visible: true, item: defaultForm })
                }
              />
            </Col>
          </Row>
        </BoxStyled>
        <div>
          <Spin spinning={loading} tip="กำลังโหลดข้อมูล...">
            {loading ? (
              <Skeleton active paragraph={{ rows: 10 }}></Skeleton>
            ) : (
              <div style={{ marginTop: "1rem" }}>
                <Row gutter={[16, 16]} align="middle">
                  {dataSource.map((item: IBrands) => (
                    <Col xl={8} key={item.id}>
                      <CardStyled
                        actions={[
                          <DivAction>
                            <CardAction
                              text="แก้ไขรายการ"
                              onClick={() => onEditItem(item)}
                              type="edit"
                            />
                          </DivAction>,
                          <DivAction>
                            <CardAction
                              text="ลบรายการ"
                              onClick={() => onDeleteItem(item.id)}
                              type="delete"
                            />
                          </DivAction>,
                        ]}
                      >
                        <Row gutter={[8, 8]}>
                          <ColName xl={19}>
                            <div>
                              <TextBrand>ชื่อแบรนด์</TextBrand>
                              <SpanTitle>{item.name}</SpanTitle>
                            </div>
                          </ColName>
                          <Col xl={5}>
                            <ImageBrands
                              alt="img-brands"
                              src={renderImage(item.image_file)}
                            />
                          </Col>
                        </Row>
                      </CardStyled>
                    </Col>
                  ))}
                </Row>
              </div>
            )}
          </Spin>
        </div>
      </div>
    </div>
  );
}

const ImageBrands = styled.img`
  width: 100%;
  height: 3rem;
  border-radius: 3px;
`;
const ColName = styled(Col)`
  display: flex;
  align-items: center;
`;
const SpanTitle = styled.span`
  font-size: 14px;
  font-weight: 500;
  margin-right: 1rem;
`;
const TextBrand = styled.p`
  margin-bottom: 0px;
  font-size: 12px;
  color: grey;
`;
const CardStyled = styled(Card)`
  width: 100%;
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  padding: 0px 7px 0px 7px;
  background: white;
  & .ant-card-body {
    padding: 12px;
  }
`;
const DivAction = styled.div`
  display: flex;
  justify-content: center;
  align-item: center;
  padding: 0px 7px;
`;
