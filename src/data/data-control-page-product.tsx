import { IPageProduct } from "src/interfaces/interface-table";
import {
  mockDataAccessery,
  mockDataAtom,
  mockDataBattery,
  mockDataBox,
  mockDataCoil,
  mockDataFreebase,
  mockDataMod,
  mockDataPod,
  mockDataSaltnic,
  mockDataStoreLiquid,
  mockDataWire,
  mockDataProductSet,
  mockDataProductCustom,
} from "src/mock/mock-row-table-product";

export const ControlProductSet: IPageProduct = {
  name: "เซ็ตพร้อมสูบ",
  single: false,
  set: true,
  freebase: false,
  saltnic: false,
  mockData: mockDataProductSet,
  dataSourceExpand: [],
};
export const ControlBox: IPageProduct = {
  name: "กล่อง",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataBox,
  dataSourceExpand: mockDataProductCustom,
};
export const ControlMod: IPageProduct = {
  name: "มอต",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataMod,
  dataSourceExpand: mockDataProductCustom,
};
export const ControlPod: IPageProduct = {
  name: "พอต",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataPod,
  dataSourceExpand: mockDataProductCustom,
};
export const ControlFreebase: IPageProduct = {
  name: "น้ำยา Freebase",
  single: false,
  set: false,
  freebase: true,
  saltnic: false,
  mockData: mockDataFreebase,
  dataSourceExpand: [],
};
export const ControlSaltnic: IPageProduct = {
  name: "น้ำยา Saltnic",
  single: false,
  set: false,
  freebase: false,
  saltnic: true,
  mockData: mockDataSaltnic,
  dataSourceExpand: [],
};

export const ControlStoreLiquid: IPageProduct = {
  name: "น้ำยาทางร้าน",
  single: false,
  set: false,
  freebase: true,
  saltnic: true,
  mockData: mockDataStoreLiquid,
  dataSourceExpand: [],
};

export const ControlAtom: IPageProduct = {
  name: "อะตอม",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataAtom,
  dataSourceExpand: mockDataProductCustom,
};
export const ControlCoil: IPageProduct = {
  name: "คอยล์",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataCoil,
  dataSourceExpand: [],
};
export const ControlCharger: IPageProduct = {
  name: "ถ่าน รางชาร์ท",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataBattery,
  dataSourceExpand: [],
};
export const ControlWire: IPageProduct = {
  name: "สำลี ลวด",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataWire,
  dataSourceExpand: [],
};
export const ControlAccessary: IPageProduct = {
  name: "อุปกรณ์ต่างๆ",
  single: true,
  set: false,
  freebase: false,
  saltnic: false,
  mockData: mockDataAccessery,
  dataSourceExpand: [],
};
