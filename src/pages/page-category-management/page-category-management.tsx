import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Typography,
  Input,
  Card,
  Spin,
  Modal,
  Skeleton,
  Collapse,
  Space,
  Button,
  message,
  Empty,
  Popconfirm,
} from "antd";
import {
  HeaderPage,
  ButtonCustom,
  BoxStyled,
  CardAction,
  TagStatus,
} from "src/components";
import styled from "styled-components";

import { API, AXIOS } from "src/services";
import { AxiosError, AxiosResponse } from "axios";
import { ICategories } from "src/interfaces/interface-response";
import { messageAction } from "src/utils/message";
import { propsModalDelete } from "src/utils/props-component";
import ModalFormMianCategory from "./modal-manage-main-cate";
import { PlusCircleOutlined } from "@ant-design/icons";
import ModalFormSubCategory from "./modal-form-add-sub";
import ModalFormCategory from "./modal-form-category";

const { Panel } = Collapse;
const { confirm } = Modal;
const { Title } = Typography;

interface IPropsPanel {
  item: ICategories;
  hasChildren: boolean;
  level: number;
}
interface IModalProps {
  visible: "main" | "sub" | "";
  parent: ICategories;
}
export default function PageOrderManagement() {
  const defaultParentItem: ICategories = {
    name: "",
    is_active: false,
    id: NaN,
    parent_id: NaN,
    sub_categories: [],
  };
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<ICategories[]>([]);
  const [modalProps, setModalProps] = useState<IModalProps>({
    visible: "",
    parent: defaultParentItem,
  });
  const [modalPropsEdit, setModalPropsEdit] = useState({
    visible: false,
    propsFormItem: defaultParentItem,
    type: "edit-parent",
  });

  const onFetchData = (search?: string) => {
    setLoading(true);
    AXIOS({
      method: "get",
      url: API.CATEGORY,
      params: { name: search ?? "" },
    })
      .then(({ data }: AxiosResponse<any, any>) => {
        const response: ICategories[] = data.data;
        setData(response);
        setLoading(false);
      })
      .catch((error: AxiosError<any, any>) => {
        messageAction("getError", error.message);
        setLoading(false);
      });
  };
  const onDeleteItem = (id: number, is_parent: boolean) => {
    confirm({
      ...propsModalDelete,
      onOk() {
        return new Promise((resolve: any, reject: any) => {
          AXIOS({
            method: "delete",
            url: API.CATEGORY,
            params: { id, is_parent },
          })
            .then(() => {
              onFetchData();
              resolve();
              messageAction("deleteSuccess");
            })
            .catch((error: AxiosError<any, any>) => {
              reject();
            });
        }).catch(() => {
          messageAction("deleteFail");
        });
      },
    });
  };

  useEffect(onFetchData, []);

  const onAddMainItem = () => {
    setModalProps({
      visible: "main",
      parent: defaultParentItem,
    });
  };
  const onAddSubCatergory = (parent: ICategories) => {
    setModalProps({
      visible: "sub",
      parent,
    });
  };
  const onCloseModal = () => {
    setModalProps({
      parent: defaultParentItem,
      visible: "",
    });
    setModalPropsEdit({
      visible: false,
      propsFormItem: defaultParentItem,
      type: "edit-parent",
    });
  };

  const onFinishedModal = () => {
    onCloseModal();
    onFetchData();
  };
  const onEditItem = (item: ICategories) => {
    setModalPropsEdit({
      visible: true,
      propsFormItem: item,
      type: "edit-parent",
    });
  };
  const PanelItem = ({
    item,
    level,
  }: {
    item: ICategories;
    level: number;
    key: number;
  }) => {
    const hasChildren: boolean =
      item.sub_categories && item.sub_categories.length !== 0;
    return (
      <div>
        <CollapItem hasChildren={hasChildren} level={level} item={item} />
      </div>
    );
  };
  const CollapItem = (propsitem: IPropsPanel) => {
    const { hasChildren, item, level } = propsitem;
    const isNotEmpty = hasChildren ?? false;
    return (
      <div style={{ paddingLeft: `${level * 5}px`, margin: "5px" }}>
        <CollapseStyle>
          <Panel
            header={
              <div>
                <span
                  style={{ marginRight: "5px" }}
                >{`ชื่อ: ${item.name}`}</span>
                <span
                  style={{
                    marginRight: "5px",
                    fontSize: "12px",
                    color: "grey",
                  }}
                >{`( ${item.sub_categories.length} รายการย่อย)`}</span>
              </div>
            }
            key={item.id}
            extra={
              <Space
                onClick={(event: any) => {
                  event.stopPropagation();
                }}
              >
                <TagStatus status={item.is_active ? "active" : "inactive"} />
                <ButtonText
                  style={{ color: "#5c68ff" }}
                  icon={<PlusCircleOutlined />}
                  type="text"
                  size="small"
                  onClick={(event: any) => {
                    onAddSubCatergory(item);
                    event.stopPropagation();
                  }}
                >
                  เพิ่มรายการ
                </ButtonText>
                <ButtonText
                  style={{ color: "#3490FF" }}
                  size="small"
                  type="text"
                  danger
                  onClick={() => onEditItem(item)}
                >
                  แก้ไข
                </ButtonText>
                <Popconfirm
                  title="ต้องการลบ?"
                  onConfirm={() =>
                    onDeleteItem(item.id, item.parent_id === 0 ? true : false)
                  }
                  okText="ตกลง"
                  cancelText="ยกเลิก"
                >
                  <ButtonText size="small" type="text" danger>
                    ลบ
                  </ButtonText>
                </Popconfirm>
              </Space>
            }
          >
            {isNotEmpty ? (
              <Tree props={item.sub_categories ?? []} />
            ) : (
              <Empty
                description="ยังไม่มีรายการย่อยในประเภทนี้"
                style={{ margin: "15px 0" }}
                image={Empty.PRESENTED_IMAGE_SIMPLE}
              />
            )}
          </Panel>
        </CollapseStyle>
      </div>
    );
  };
  const Tree = ({ props }: any) => {
    return (
      <div>
        {props.map((item: any) => (
          <PanelItem key={item.id} item={item} level={0} />
        ))}
      </div>
    );
  };

  return (
    <div>
      <HeaderPage
        header="จัดการประเภทรายการสินค้า"
        subHeader="สามารถเพิ่มรายการประเภทสินค้า เพิ่มได้ / ลบ / เพิ่มซับ"
      />
      <div className="App-paper-page">
        <BoxStyled color="white">
          <Row align="middle">
            <Col xl={4}>
              <Title level={5}>ประเภทสินค้าทั้งหมด</Title>
            </Col>
            <Col xl={15}>
              <Input.Search
                allowClear
                onSearch={(value: string) => {
                  onFetchData(value);
                }}
              />
            </Col>
            <Col offset={1} xl={2}>
              <ButtonCustom
                type="primary"
                icon="add"
                text="เพิ่มประเภทสินค้า"
                onClick={onAddMainItem}
              />
            </Col>
          </Row>
        </BoxStyled>
        <Spin spinning={loading} tip="กำลังโหลดข้อมูล...">
          {loading ? (
            <Skeleton active paragraph={{ rows: 10 }}></Skeleton>
          ) : (
            <div style={{ marginTop: "1rem" }}>
              <CardStyled>
                <Tree props={data} />
              </CardStyled>
            </div>
          )}
        </Spin>
        <ModalFormSubCategory
          modalProps={modalProps}
          onCloseModal={onCloseModal}
          onFinishedModal={onFinishedModal}
        />
        <ModalFormMianCategory
          modalProps={modalProps}
          onCloseModal={onCloseModal}
          onFinished={onFinishedModal}
        />
        <ModalFormCategory
          modalProps={modalPropsEdit}
          onCloseModal={onCloseModal}
          onFinishedModal={onFinishedModal}
        />
      </div>
    </div>
  );
}

const ButtonText = styled(Button)`
  font-size: 12px;
`;

const CollapseStyle = styled(Collapse)`
  border-radius: 5px;
  &.ant-collapse > .ant-collapse-item > .ant-collapse-header {
    padding: 5px 16px;
    font-size: 14px;
  }
  & .ant-collapse-content > .ant-collapse-content-box {
    padding: 5px;
  }
`;
const CardStyled = styled(Card)`
  width: 100%;
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  padding: 20px;
  background: white;
  & .ant-card-body {
    padding: 10px 10px;
  }
`;
const DivAction = styled.div`
  display: flex;
  justify-content: center;
  align-item: center;
  padding: 0px 7px;
`;
